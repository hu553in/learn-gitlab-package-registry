# Learn GitLab package registry

[Package registry](https://gitlab.com/hu553in/learn-gitlab-package-registry/-/packages)

## Notes

When we publish package of the same version several times, the package registry accumulates duplicate files
that need to be deleted manually via the web interface or API.

## How to publish to registry

1. Manually via `./gradlew publish`
2. Automatically via GitLab CI
