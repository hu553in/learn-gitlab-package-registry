package com.github.hu553in.learn_gitlab_package_registry;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LibraryTest {
    @Test
    public void someLibraryMethodReturnsTrue() {
        Library classUnderTest = new Library();
        assertTrue("someLibraryMethod should return 'true'", classUnderTest.someLibraryMethod());
    }
}
